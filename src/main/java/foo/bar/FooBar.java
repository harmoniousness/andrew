package foo.bar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

// The test class we created
public class FooBar {
    private final int foo;
    private List<Integer> myList = new ArrayList<>();
    private static final Logger logger = LogManager.getLogger(FooBar.class);

    public FooBar(int f) {
        this.foo = f;
        logger.info("Created a {}", ()->"FooBar Object");
    }

    public int getFoo() {
        return foo;
    }

    public int getComplexFoo(int x) {
        int prev = 0;
        if (myList.size()>0) prev = myList.get(myList.size()-1);
        myList.add(x);
        logger.info("In GetComplexFoo with {} and {}",x,prev);
        return foo / 2 * prev;
    }
}
