package foo.bar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FooMain {
    private static final Logger logger = LogManager.getLogger(FooMain.class);

    public static void main(String [] argv) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        FooService service = context.getBean(FooService.class);
        logger.info("Starting");
        service.start();
        logger.info(service.getFoo());
    }
}
