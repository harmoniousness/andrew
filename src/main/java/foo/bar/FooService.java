package foo.bar;

public interface FooService {
    public void start();
    public String getFoo();
}
