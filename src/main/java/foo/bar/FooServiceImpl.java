package foo.bar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;

public class FooServiceImpl implements FooService {
    private final Integer p;
    private DataSource dataSource;
    private static final Logger logger = LogManager.getLogger(FooMain.class);


    public FooServiceImpl(int param) {
        this.p = param;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override public void start() {
        try (Connection c = dataSource.getConnection();
             Statement s = c.createStatement()){
            s.execute("Create Table foo ( tag varchar, n int )");
        }
        catch (Exception e) {
            logger.error("Problem creating table");
            logger.error(e);
        }
    }

    @Override public String getFoo() {
        try {
            Connection c = dataSource.getConnection();

        }
        catch (Exception e) {
            logger.error(e);
            return e.getMessage();
        }
        return "Implementation of Foo with "+p;
    }
}
