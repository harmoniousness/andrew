package foo.bar;

public class FooServiceTestImpl implements FooService {
    @Override public void start() {}

    @Override public String getFoo() {
        return "Test";
    }
}
