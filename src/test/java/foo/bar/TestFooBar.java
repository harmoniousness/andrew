package foo.bar;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFooBar {

    // First test case
    @Test
    @DisplayName("A basic test")
    public void testSimpleFB() {
        FooBar f = new FooBar(7);
        assertAll(
                ()-> assertEquals(7,f.getFoo())
        );
    }

    // Second test case
    @Test
    @DisplayName("A complex test")
    public void testComplexFB() {
        FooBar f = new FooBar(7);
        assertAll(
                ()-> assertEquals(0,f.getComplexFoo(1)),
                ()-> assertEquals(3,f.getComplexFoo(2)),
                ()-> assertEquals(6,f.getComplexFoo(3)),
                ()-> assertEquals(9,f.getComplexFoo(4))
        );
    }

}
