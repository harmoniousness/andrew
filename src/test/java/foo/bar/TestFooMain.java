package foo.bar;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;

public class TestFooMain {
    @Test
    public void testFooMain() {
        ApplicationContext context = new ClassPathXmlApplicationContext("testcontext.xml");
        FooService service = context.getBean(FooService.class);
        String result = service.getFoo();
        assertEquals("Test",result);
    }
}
